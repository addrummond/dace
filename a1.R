library(plyr);
library(ggplot2);
library(grid);
library(gridExtra);
library(MASS);
library(gdata);
#library(lme4);

multiplot <- function(..., plotlist=NULL, cols) {
    require(grid)

    # Make a list from the ... arguments and plotlist
    plots <- c(list(...), plotlist)

    numPlots = length(plots)

    # Make the panel
    plotCols = cols                          # Number of columns of plots
    plotRows = ceiling(numPlots/plotCols) # Number of rows needed, calculated from # of cols

    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(plotRows, plotCols)))
    vplayout <- function(x, y)
        viewport(layout.pos.row = x, layout.pos.col = y)

    # Make each plot, in the correct location
    for (i in 1:numPlots) {
        curRow = ceiling(i/plotCols)
        curCol = (i-1) %% plotCols + 1
        print(plots[[i]], vp = vplayout(curRow, curCol ))
    }

}

results <- read.csv("~/dace/results1.txt", comment.char="#", fill=TRUE, header=FALSE);

judgments <- results[results[[3]]=='MyAcceptabilityJudgment',];

# Merge adjacent MyAcceptabilityJudgment rows into single rows.
judgments$n <- seq.int(nrow(judgments));
answers <- judgments[judgments$n %% 2 == 0,];
sentences <- judgments[judgments$n %% 2 == 1,];
answers$n2 <- seq.int(nrow(answers));
sentences$n2 <- seq.int(nrow(sentences));

judgments <- merge(sentences, answers, by="n2");
judgments <- rename(judgments, c("V9.y"="answer", "V6.x"="type", "V8.x"="sentence", "V4.x"="item", "V2.x"="subject"));
judgments$answer <- factor(judgments$answer, levels=c("Yes","No"));

# Filter out judgments from people who got the exemplars wrong (except for the
# last one).
good_subjects1 <- judgments[judgments$item==8 & judgments$answer == "No",]$subject;
good_subjects2 <- judgments[judgments$item==9 & judgments$answer == "Yes",]$subject;
good_subjects3 <- judgments[judgments$item==10 & judgments$answer == "Yes",]$subject;
good_subjects <- unique(judgments[(judgments$subject %in% good_subjects1) & (judgments$subject %in% good_subjects2) & (judgments$subject %in% good_subjects3),]$V2.y);

judgments <- judgments[judgments$V2.y %in% good_subjects,];

cont <- with(judgments, table(answer,type))

conditions <- c("ac1", "ac2", "ac3", "ac4", "pc1", "pc2", "pc3", "pc4");
examples <- c(
    "Which N of X does Xprn verb?",
    "Which N of X does Xprn think that Y verbs?",
    "Which N of X's Y does Xprn verb?",
    "Which N of X does Y think Xprn verb?",
    "How Adj of X is Xprn?",
    "How Adj of X does Xprn think Y is?",
    "How Adj of X's Y is Xprn?",
    "How Adj of X does Y think Xprn is?"
);

arg <- c("ac1","ac2","ac3","ac4");
pred <- c("pc1","pc2","pc2","pc4");
judgments <- transform(judgments, bigcond = ifelse(type %in% arg, "arg", ifelse(type %in% pred, "pred", "neither")))
judgments <- transform(
    judgments,
    n = ifelse(answer == "Yes", 1, 0),
    embedded_n = ifelse(type == "ac1" | type == "ac4" | type == "pc1" | type == "pc4", 0, 1),
    argument_n = ifelse(type == "ac1" | type == "ac2" | type == "ac3" | type == "ac4", 1, 0),
    pemb_n = ifelse(type == "ac3" | type == "pc3", 1, 0),
   cemb_n = ifelse(type == "ac2" | type == "pc2", 1, 0),
    long_n = ifelse(type == "ac4" | type == "pc4", 1, 0),
    side_n = ifelse(regexpr("(side)|(friend)|(enemy)", sentence) > 0, 1, 0)
);

make_big_plot <- function() {
    plots <- lapply(seq.int(length(conditions)), function (i) {
        ggplot(data=judgments[judgments$type==conditions[i],], aes(x=answer)) + geom_bar() + xlab(examples[i]) + ylab("") + scale_x_discrete(drop=FALSE);
    });

    multiplot(plotlist=plots,cols=2);
}

make_predarg_plot <- function () {
    js <- transform(judgments, a = ifelse(answer == "Yes", "Coref", "Noncoref"));

    t <- theme(text = element_text(size=16), aspect.ratio=0.5);
    arg <- ggplot(data=js[js$bigcond=="arg",], aes(x=a)) + geom_bar() + xlab("Argument") + ylab("") + scale_x_discrete(drop=FALSE) + t;
    pred <- ggplot(data=js[js$bigcond=="pred",], aes(x=a)) + geom_bar() + xlab("Predicate") + ylab("") + scale_x_discrete(drop=FALSE) + t;
    multiplot(plotlist=list(arg,pred), cols=2);
}

make_slim_plot <- function () {
    js <- transform(judgments, a = ifelse(answer == "Yes", "Coref", "Noncoref"));
    t <- theme(text = element_text(size=16), aspect.ratio=0.5);
    ggplot(data=js[js$type=="ac1",], aes(x=a)) + geom_bar() + xlab("Argument (a)") + ylab("") + scale_x_discrete(drop=FALSE) + t;
}

make_argd_plot <- function () {
    js <- transform(judgments, a = ifelse(answer == "Yes", "Coref", "Noncoref"));

    t <- theme(text = element_text(size=16), aspect.ratio=0.5);
    ggplot(data=js[js$type=="ac4",], aes(x=a)) + geom_bar() + xlab("Argument (d)") + ylab("") + scale_x_discrete(drop=FALSE) + t;
}

make_split_plot <- function () {
    js <- transform(judgments, a = ifelse(answer == "Yes", "Coref", "Noncoref"));

    t <- theme(text = element_text(size=16), aspect.ratio=0.5);
    arg <- ggplot(data=js[js$type=="ac1",], aes(x=a)) + geom_bar() + xlab("Argument (a)") + ylab("") + scale_x_discrete(drop=FALSE) + t;
    pred <- ggplot(data=js[js$type=="ac2",], aes(x=a)) + geom_bar() + xlab("Argument (c)") + ylab("") + scale_x_discrete(drop=FALSE) + t;
    multiplot(plotlist=list(arg,pred), cols=2);
}

make_side_plot <- function () {
    js <- transform(judgments, a = ifelse(answer == "Yes", "Coref", "Noncoref"));
    t <- theme(text = element_text(size=16), aspect.ratio=0.5);

    js <- js[js$type %in% conditions,];
    side <- js[js$side_n == 1,];
    not_side <- js[js$side_n == 0,];
    side_plot <- ggplot(data=side, aes_string(x="a")) + geom_bar() + scale_x_discrete(drop=FALSE) + xlab("side,friend,enemy") + ylab("") + t;
    not_side_plot <- ggplot(data=not_side, aes_string(x="a")) + geom_bar() + scale_x_discrete(drop=FALSE) + xlab("picture,statue") + ylab("") + t;
    multiplot(plotlist=list(side_plot,not_side_plot), cols=2);
}

make_practice_plot <- function () {
    ps <- judgments[judgments$type == "practice",];
    s1 <- ps[ps$sentence == "[He] knows that [John] will win.",];
    s2 <- ps[ps$sentence == "After [he] arrived late to the meeting%2C [Jason] apologized.",];
    s3 <- ps[ps$sentence == "While [Mary] was in town%2C [she] visited an old friend.",];
    s4 <- ps[ps$sentence == "[Her] friends believe that [Jane] is intelligent.",];

    t <- theme(text = element_text(size=8));

    s1p <- ggplot(data=s1, aes(x=answer)) + geom_bar() + scale_x_discrete(drop=FALSE) + xlab("He knows that John will win.") + t;
    s2p <- ggplot(data=s2, aes(x=answer)) + geom_bar() + scale_x_discrete(drop=FALSE) + xlab("After he arrived late to the meeting, Jason apologized.") + t;
    s3p <- ggplot(data=s3, aes(x=answer)) + geom_bar() + scale_x_discrete(drop=FALSE) + xlab("While Mary was in town, she visited an old friend.") + t;
    s4p <- ggplot(data=s4, aes(x=answer)) + geom_bar() + scale_x_discrete(drop=FALSE) + xlab("Her friends believe that Jane is intelligent.") + t;
    multiplot(plotlist=list(s1p,s2p,s3p,s4p), cols=2);
}

do_predicate_vs_argument_chi <- function () {
    chisq.test(with(drop.levels(judgments[judgments$bigcond == "arg" | judgments$bigcond == "pred",]), table(bigcond, answer)))
}

do_main_lr <- function () {
    summary(glmer(n ~ argument_n + pemb_n + cemb_n + long_n + (1|subject) + (1|item), family=binomial, data=judgments[judgments$type %in% conditions,]))
}

do_embedded_vs_non_lr <- function () {

}
