Experiment 1
==========

    N subjects before exclusion: 61
    After exclusions: 54

Distribution of latin squares (before exclusions) was:

    Square 0: 18 subjects
    Square 1: 11 subjects
    Square 2: 16 subjects
    Square 3: 16 subjects

Note that because of this uneven distribution, the total number of responses for each condition is not exactly the same.

(Num coref responses first, noncoref second)

    CHANGE SENTENCES IN PAPER
    6a:  6,48   "He saw that enemy of Superman's parner"
    6b:  10,44  "He thinks Lois saw that enemy of Superman"

    9a:  6, 64
    9b:  15, 55
    9c:  24, 42

    14a: 21, 49
    14b: 34, 30 [CORRECTED, previously said 34, 40]
    14c: 45, 25


Experiment 2
==========

    N subjects before exclusions: 99
    After exclusions: 92

Distribution of latin squares (before exclusions) was:

    Square 0: 17 subjects
    Square 1: 12 subjects
    Square 2: 23 subjects
    Square 3: 31 subjects
    Square 4: 16 subjects

Raw numbers:

    18: 54,38
    19: 75,17
    20: 76,16


Experiment 3
==========

    N subjects before exclusions: 100
    After exclusions: 89

Distribution of latin squares (before exclusions) was:

    Square 0: 9 subjects
    Square 1: 23 subjects
    Square 2: 20 subjects
    Square 3: 24 subjects
    Square 4: 15 subjects
    Square 5: 9 subjects

Note that the numbers below are a bit lower than might be expected given the number of subjects because we had 5 items and 6 conditions (so that there is slightly less than one response per subject for each condition).

    25a: 44,24
    25b: 61,15

    24a: 60, 8
    24b: 62, 9

    26a: 54, 26
    26b: 67, 15

